EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Config_Card-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_02x04_Odd_Even J1
U 1 1 5AD0D736
P 4550 3100
F 0 "J1" H 4600 3300 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 4600 2800 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecTMM" H 4550 3100 50  0001 C CNN
F 3 "" H 4550 3100 50  0001 C CNN
	1    4550 3100
	1    0    0    -1  
$EndComp
Text Label 4050 3000 0    60   ~ 0
V0
Wire Wire Line
	4350 3000 3900 3000
$Comp
L Conn_02x04_Odd_Even J3
U 1 1 5AD0D82D
P 6150 3100
F 0 "J3" H 6200 3300 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 6200 2800 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecTMM" H 6150 3100 50  0001 C CNN
F 3 "" H 6150 3100 50  0001 C CNN
	1    6150 3100
	1    0    0    -1  
$EndComp
Text Label 5650 3000 0    60   ~ 0
V2
Wire Wire Line
	5950 3000 5500 3000
$Comp
L Conn_02x04_Odd_Even J2
U 1 1 5AD0D881
P 4550 3700
F 0 "J2" H 4600 3900 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 4600 3400 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecTMM" H 4550 3700 50  0001 C CNN
F 3 "" H 4550 3700 50  0001 C CNN
	1    4550 3700
	1    0    0    -1  
$EndComp
Text Label 4050 3600 0    60   ~ 0
V1
Wire Wire Line
	4350 3600 3900 3600
$Comp
L Conn_02x04_Odd_Even J4
U 1 1 5AD0D897
P 6150 3700
F 0 "J4" H 6200 3900 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 6200 3400 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecTMM" H 6150 3700 50  0001 C CNN
F 3 "" H 6150 3700 50  0001 C CNN
	1    6150 3700
	1    0    0    -1  
$EndComp
Text Label 5650 3600 0    60   ~ 0
V3
Wire Wire Line
	5950 3600 5500 3600
$Comp
L Conn_02x08_Odd_Even J6
U 1 1 5AD0D9C4
P 8900 4550
F 0 "J6" H 8950 4950 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 8950 4050 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x08_Pitch2.00mm_samtecTMM" H 8900 4550 50  0001 C CNN
F 3 "" H 8900 4550 50  0001 C CNN
	1    8900 4550
	1    0    0    -1  
$EndComp
NoConn ~ 8700 3050
NoConn ~ 8700 3150
NoConn ~ 8700 3750
NoConn ~ 8700 4250
NoConn ~ 8700 4350
NoConn ~ 8700 4850
NoConn ~ 8700 4950
NoConn ~ 8700 4450
NoConn ~ 8700 4550
NoConn ~ 8700 4650
NoConn ~ 8700 4750
NoConn ~ 9200 4250
NoConn ~ 9200 4350
NoConn ~ 9200 4450
NoConn ~ 9200 4550
NoConn ~ 9200 4650
NoConn ~ 9200 4750
NoConn ~ 9200 4850
NoConn ~ 9200 4950
$Comp
L Conn_02x08_Odd_Even J5
U 1 1 5AD0D973
P 8900 3350
F 0 "J5" H 8950 3750 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 8950 2850 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x08_Pitch2.00mm_samtecTMM" H 8900 3350 50  0001 C CNN
F 3 "" H 8900 3350 50  0001 C CNN
	1    8900 3350
	1    0    0    -1  
$EndComp
NoConn ~ 8700 3250
NoConn ~ 8700 3350
NoConn ~ 8700 3450
NoConn ~ 8700 3550
NoConn ~ 8700 3650
Text Label 4050 3100 0    60   ~ 0
V0
Wire Wire Line
	4350 3100 3900 3100
Text Label 4050 3200 0    60   ~ 0
V0
Wire Wire Line
	4350 3200 3900 3200
Text Label 4050 3300 0    60   ~ 0
V0
Wire Wire Line
	4350 3300 3900 3300
Text Label 5000 3000 0    60   ~ 0
V0
Wire Wire Line
	5300 3000 4850 3000
Text Label 5000 3100 0    60   ~ 0
V0
Wire Wire Line
	5300 3100 4850 3100
Text Label 5000 3200 0    60   ~ 0
V0
Wire Wire Line
	5300 3200 4850 3200
Text Label 5000 3300 0    60   ~ 0
V0
Wire Wire Line
	5300 3300 4850 3300
Text Label 5650 3100 0    60   ~ 0
V2
Wire Wire Line
	5950 3100 5500 3100
Text Label 5650 3200 0    60   ~ 0
V2
Wire Wire Line
	5950 3200 5500 3200
Text Label 5650 3300 0    60   ~ 0
V2
Wire Wire Line
	5950 3300 5500 3300
Text Label 6600 3000 0    60   ~ 0
V2
Wire Wire Line
	6900 3000 6450 3000
Text Label 6600 3100 0    60   ~ 0
V2
Wire Wire Line
	6900 3100 6450 3100
Text Label 6600 3200 0    60   ~ 0
V2
Wire Wire Line
	6900 3200 6450 3200
Text Label 6600 3300 0    60   ~ 0
V2
Wire Wire Line
	6900 3300 6450 3300
Text Label 5650 3700 0    60   ~ 0
V3
Wire Wire Line
	5950 3700 5500 3700
Text Label 5650 3800 0    60   ~ 0
V3
Wire Wire Line
	5950 3800 5500 3800
Text Label 5650 3900 0    60   ~ 0
V3
Wire Wire Line
	5950 3900 5500 3900
Text Label 6600 3600 0    60   ~ 0
V3
Wire Wire Line
	6900 3600 6450 3600
Text Label 6600 3700 0    60   ~ 0
V3
Wire Wire Line
	6900 3700 6450 3700
Text Label 6600 3800 0    60   ~ 0
V3
Wire Wire Line
	6900 3800 6450 3800
Text Label 6600 3900 0    60   ~ 0
V3
Wire Wire Line
	6900 3900 6450 3900
Text Label 4050 3700 0    60   ~ 0
V1
Wire Wire Line
	4350 3700 3900 3700
Text Label 4050 3800 0    60   ~ 0
V1
Wire Wire Line
	4350 3800 3900 3800
Text Label 4050 3900 0    60   ~ 0
V1
Wire Wire Line
	4350 3900 3900 3900
Text Label 5000 3600 0    60   ~ 0
V1
Wire Wire Line
	5300 3600 4850 3600
Text Label 5000 3700 0    60   ~ 0
V1
Wire Wire Line
	5300 3700 4850 3700
Text Label 5000 3800 0    60   ~ 0
V1
Wire Wire Line
	5300 3800 4850 3800
Text Label 5000 3900 0    60   ~ 0
V1
Wire Wire Line
	5300 3900 4850 3900
Text Label 9350 3050 0    60   ~ 0
V0
Wire Wire Line
	9650 3050 9200 3050
Text Label 9350 3150 0    60   ~ 0
V0
Wire Wire Line
	9650 3150 9200 3150
Text Label 9350 3250 0    60   ~ 0
V1
Wire Wire Line
	9650 3250 9200 3250
Text Label 9350 3350 0    60   ~ 0
V1
Wire Wire Line
	9650 3350 9200 3350
Text Label 9350 3450 0    60   ~ 0
V2
Wire Wire Line
	9650 3450 9200 3450
Text Label 9350 3550 0    60   ~ 0
V2
Wire Wire Line
	9650 3550 9200 3550
Text Label 9350 3650 0    60   ~ 0
V3
Wire Wire Line
	9650 3650 9200 3650
Text Label 9350 3750 0    60   ~ 0
V3
Wire Wire Line
	9650 3750 9200 3750
$EndSCHEMATC
