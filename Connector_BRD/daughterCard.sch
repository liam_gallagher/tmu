EESchema Schematic File Version 2
LIBS:conn_brd-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:conn_brd-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_02x08_Odd_Even J4
U 1 1 5ADACD16
P 4050 3900
AR Path="/5AE0048B/5ADACD16" Ref="J4"  Part="1" 
AR Path="/5AE01390/5ADACD16" Ref="J11"  Part="1" 
AR Path="/5AE0244B/5ADACD16" Ref="J18"  Part="1" 
AR Path="/5AE03F44/5ADACD16" Ref="J25"  Part="1" 
AR Path="/5AE04D35/5ADACD16" Ref="J32"  Part="1" 
AR Path="/5AE0593D/5ADACD16" Ref="J39"  Part="1" 
AR Path="/5AE05E7C/5ADACD16" Ref="J46"  Part="1" 
AR Path="/5AE07945/5ADACD16" Ref="J53"  Part="1" 
AR Path="/5AE094A1/5ADACD16" Ref="J60"  Part="1" 
AR Path="/5AE0AD1B/5ADACD16" Ref="J67"  Part="1" 
AR Path="/5AE0C60A/5ADACD16" Ref="J74"  Part="1" 
AR Path="/5AE0F6A9/5ADACD16" Ref="J81"  Part="1" 
F 0 "J81" H 4100 4300 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 4100 3400 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x08_Pitch2.00mm_samtecSMM" H 4050 3900 50  0001 C CNN
F 3 "" H 4050 3900 50  0001 C CNN
	1    4050 3900
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x08_Odd_Even J5
U 1 1 5ADACD89
P 6150 3900
AR Path="/5AE0048B/5ADACD89" Ref="J5"  Part="1" 
AR Path="/5AE01390/5ADACD89" Ref="J12"  Part="1" 
AR Path="/5AE0244B/5ADACD89" Ref="J19"  Part="1" 
AR Path="/5AE03F44/5ADACD89" Ref="J26"  Part="1" 
AR Path="/5AE04D35/5ADACD89" Ref="J33"  Part="1" 
AR Path="/5AE0593D/5ADACD89" Ref="J40"  Part="1" 
AR Path="/5AE05E7C/5ADACD89" Ref="J47"  Part="1" 
AR Path="/5AE07945/5ADACD89" Ref="J54"  Part="1" 
AR Path="/5AE094A1/5ADACD89" Ref="J61"  Part="1" 
AR Path="/5AE0AD1B/5ADACD89" Ref="J68"  Part="1" 
AR Path="/5AE0C60A/5ADACD89" Ref="J75"  Part="1" 
AR Path="/5AE0F6A9/5ADACD89" Ref="J82"  Part="1" 
F 0 "J82" H 6200 4300 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 6200 3400 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x08_Pitch2.00mm_samtecSMM" H 6150 3900 50  0001 C CNN
F 3 "" H 6150 3900 50  0001 C CNN
	1    6150 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3600 3200 3600
Text HLabel 3200 3600 0    60   Input ~ 0
V0
Wire Wire Line
	3850 3700 3200 3700
Wire Wire Line
	3850 3800 3200 3800
Wire Wire Line
	3850 3900 3200 3900
Wire Wire Line
	3850 4000 3200 4000
Wire Wire Line
	3850 4100 3200 4100
Wire Wire Line
	3850 4200 3200 4200
Wire Wire Line
	3850 4300 3200 4300
Text HLabel 3200 3800 0    60   Input ~ 0
GND
Text HLabel 3200 3900 0    60   Input ~ 0
GND
Text HLabel 3200 4000 0    60   Input ~ 0
GND
Text HLabel 3200 4100 0    60   Input ~ 0
GND
Text HLabel 3200 4200 0    60   Input ~ 0
SDA
Text HLabel 3200 4300 0    60   Input ~ 0
SCL
Text HLabel 3200 3700 0    60   Input ~ 0
V1
NoConn ~ 4350 3600
NoConn ~ 4350 3700
NoConn ~ 4350 3800
NoConn ~ 4350 3900
NoConn ~ 4350 4000
NoConn ~ 4350 4100
NoConn ~ 4350 4200
NoConn ~ 4350 4300
Wire Wire Line
	5950 3600 5600 3600
Text Label 5700 3600 0    60   ~ 0
1
Wire Wire Line
	5950 3700 5600 3700
Text Label 5700 3700 0    60   ~ 0
2
Wire Wire Line
	5950 3800 5600 3800
Text Label 5700 3800 0    60   ~ 0
3
Wire Wire Line
	5950 3900 5600 3900
Text Label 5700 3900 0    60   ~ 0
4
Wire Wire Line
	5950 4000 5600 4000
Text Label 5700 4000 0    60   ~ 0
5
Wire Wire Line
	5950 4100 5600 4100
Text Label 5700 4100 0    60   ~ 0
6
Wire Wire Line
	5950 4200 5600 4200
Text Label 5700 4200 0    60   ~ 0
7
Wire Wire Line
	5950 4300 5600 4300
Text Label 5700 4300 0    60   ~ 0
8
Wire Wire Line
	6800 3600 6450 3600
Text Label 6550 3600 0    60   ~ 0
9
Wire Wire Line
	6800 3700 6450 3700
Text Label 6550 3700 0    60   ~ 0
10
Wire Wire Line
	6800 3800 6450 3800
Text Label 6550 3800 0    60   ~ 0
11
Wire Wire Line
	6800 3900 6450 3900
Text Label 6550 3900 0    60   ~ 0
12
Wire Wire Line
	6800 4000 6450 4000
Text Label 6550 4000 0    60   ~ 0
13
Wire Wire Line
	6800 4100 6450 4100
Text Label 6550 4100 0    60   ~ 0
14
Wire Wire Line
	4700 2400 4350 2400
Text Label 4450 2400 0    60   ~ 0
1
Wire Wire Line
	4700 2500 4350 2500
Text Label 4450 2500 0    60   ~ 0
2
Wire Wire Line
	4700 2600 4350 2600
Text Label 4450 2600 0    60   ~ 0
3
Wire Wire Line
	4700 2700 4350 2700
Text Label 4450 2700 0    60   ~ 0
4
Wire Wire Line
	4700 2800 4350 2800
Text Label 4450 2800 0    60   ~ 0
5
Wire Wire Line
	4700 2900 4350 2900
Text Label 4450 2900 0    60   ~ 0
6
Wire Wire Line
	4700 3000 4350 3000
Text Label 4450 3000 0    60   ~ 0
7
Wire Wire Line
	5550 2400 5200 2400
Text Label 5300 2400 0    60   ~ 0
8
$Comp
L Conn_02x07_Top_Bottom J3
U 1 1 5ADB16AB
P 4900 2700
AR Path="/5AE0048B/5ADB16AB" Ref="J3"  Part="1" 
AR Path="/5AE01390/5ADB16AB" Ref="J10"  Part="1" 
AR Path="/5AE0244B/5ADB16AB" Ref="J17"  Part="1" 
AR Path="/5AE03F44/5ADB16AB" Ref="J24"  Part="1" 
AR Path="/5AE04D35/5ADB16AB" Ref="J31"  Part="1" 
AR Path="/5AE0593D/5ADB16AB" Ref="J38"  Part="1" 
AR Path="/5AE05E7C/5ADB16AB" Ref="J45"  Part="1" 
AR Path="/5AE07945/5ADB16AB" Ref="J52"  Part="1" 
AR Path="/5AE094A1/5ADB16AB" Ref="J59"  Part="1" 
AR Path="/5AE0AD1B/5ADB16AB" Ref="J66"  Part="1" 
AR Path="/5AE0C60A/5ADB16AB" Ref="J73"  Part="1" 
AR Path="/5AE0F6A9/5ADB16AB" Ref="J80"  Part="1" 
F 0 "J80" H 4950 3100 50  0000 C CNN
F 1 "Conn_02x07_Top_Bottom" H 4950 2300 50  0000 C CNN
F 2 "Connectors_Harwin:Harwin_JTek-Male_2x07x2.00mm_Straight_JS" H 4900 2700 50  0001 C CNN
F 3 "" H 4900 2700 50  0001 C CNN
	1    4900 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2500 5200 2500
Text Label 5300 2500 0    60   ~ 0
9
Wire Wire Line
	5550 2600 5200 2600
Text Label 5300 2600 0    60   ~ 0
10
Wire Wire Line
	5550 2700 5200 2700
Text Label 5300 2700 0    60   ~ 0
11
Wire Wire Line
	5550 2800 5200 2800
Text Label 5300 2800 0    60   ~ 0
12
Wire Wire Line
	5550 2900 5200 2900
Text Label 5300 2900 0    60   ~ 0
13
Wire Wire Line
	5550 3000 5200 3000
Text Label 5300 3000 0    60   ~ 0
14
Text HLabel 4800 5050 2    60   Input ~ 0
V7
Text HLabel 4800 5150 2    60   Input ~ 0
V6
Text HLabel 4800 5250 2    60   Input ~ 0
V5
Text HLabel 4800 5350 2    60   Input ~ 0
V4
Text HLabel 3200 5350 0    60   Input ~ 0
V3
Text HLabel 3200 5250 0    60   Input ~ 0
V2
Text HLabel 3200 5150 0    60   Input ~ 0
V1
Text HLabel 3200 5050 0    60   Input ~ 0
V0
Wire Wire Line
	4800 5350 4250 5350
Wire Wire Line
	4800 5250 4250 5250
Wire Wire Line
	4800 5150 4250 5150
Wire Wire Line
	4800 5050 4250 5050
Wire Wire Line
	3750 5350 3200 5350
Wire Wire Line
	3750 5250 3200 5250
Wire Wire Line
	3750 5150 3200 5150
Wire Wire Line
	3750 5050 3200 5050
$Comp
L Conn_02x04_Odd_Even J6
U 1 1 5ADB1930
P 3950 5150
AR Path="/5AE0048B/5ADB1930" Ref="J6"  Part="1" 
AR Path="/5AE01390/5ADB1930" Ref="J13"  Part="1" 
AR Path="/5AE0244B/5ADB1930" Ref="J20"  Part="1" 
AR Path="/5AE03F44/5ADB1930" Ref="J27"  Part="1" 
AR Path="/5AE04D35/5ADB1930" Ref="J34"  Part="1" 
AR Path="/5AE0593D/5ADB1930" Ref="J41"  Part="1" 
AR Path="/5AE05E7C/5ADB1930" Ref="J48"  Part="1" 
AR Path="/5AE07945/5ADB1930" Ref="J55"  Part="1" 
AR Path="/5AE094A1/5ADB1930" Ref="J62"  Part="1" 
AR Path="/5AE0AD1B/5ADB1930" Ref="J69"  Part="1" 
AR Path="/5AE0C60A/5ADB1930" Ref="J76"  Part="1" 
AR Path="/5AE0F6A9/5ADB1930" Ref="J83"  Part="1" 
F 0 "J83" H 4000 5350 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 4000 4850 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecSMM" H 3950 5150 50  0001 C CNN
F 3 "" H 3950 5150 50  0001 C CNN
	1    3950 5150
	1    0    0    -1  
$EndComp
Text HLabel 4800 5700 2    60   Input ~ 0
V7
Text HLabel 4800 5800 2    60   Input ~ 0
V6
Text HLabel 4800 5900 2    60   Input ~ 0
V5
Text HLabel 4800 6000 2    60   Input ~ 0
V4
Text HLabel 3200 6000 0    60   Input ~ 0
V3
Text HLabel 3200 5900 0    60   Input ~ 0
V2
Text HLabel 3200 5800 0    60   Input ~ 0
V1
Text HLabel 3200 5700 0    60   Input ~ 0
V0
Wire Wire Line
	4800 6000 4250 6000
Wire Wire Line
	4800 5900 4250 5900
Wire Wire Line
	4800 5800 4250 5800
Wire Wire Line
	4800 5700 4250 5700
Wire Wire Line
	3750 6000 3200 6000
Wire Wire Line
	3750 5900 3200 5900
Wire Wire Line
	3750 5800 3200 5800
Wire Wire Line
	3750 5700 3200 5700
$Comp
L Conn_02x04_Odd_Even J8
U 1 1 5ADB22B5
P 3950 5800
AR Path="/5AE0048B/5ADB22B5" Ref="J8"  Part="1" 
AR Path="/5AE01390/5ADB22B5" Ref="J15"  Part="1" 
AR Path="/5AE0244B/5ADB22B5" Ref="J22"  Part="1" 
AR Path="/5AE03F44/5ADB22B5" Ref="J29"  Part="1" 
AR Path="/5AE04D35/5ADB22B5" Ref="J36"  Part="1" 
AR Path="/5AE0593D/5ADB22B5" Ref="J43"  Part="1" 
AR Path="/5AE05E7C/5ADB22B5" Ref="J50"  Part="1" 
AR Path="/5AE07945/5ADB22B5" Ref="J57"  Part="1" 
AR Path="/5AE094A1/5ADB22B5" Ref="J64"  Part="1" 
AR Path="/5AE0AD1B/5ADB22B5" Ref="J71"  Part="1" 
AR Path="/5AE0C60A/5ADB22B5" Ref="J78"  Part="1" 
AR Path="/5AE0F6A9/5ADB22B5" Ref="J85"  Part="1" 
F 0 "J85" H 4000 6000 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 4000 5500 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecSMM" H 3950 5800 50  0001 C CNN
F 3 "" H 3950 5800 50  0001 C CNN
	1    3950 5800
	1    0    0    -1  
$EndComp
Text HLabel 6900 5050 2    60   Input ~ 0
V7
Text HLabel 6900 5150 2    60   Input ~ 0
V6
Text HLabel 6900 5250 2    60   Input ~ 0
V5
Text HLabel 6900 5350 2    60   Input ~ 0
V4
Text HLabel 5300 5350 0    60   Input ~ 0
V3
Text HLabel 5300 5250 0    60   Input ~ 0
V2
Text HLabel 5300 5150 0    60   Input ~ 0
V1
Text HLabel 5300 5050 0    60   Input ~ 0
V0
Wire Wire Line
	6900 5350 6350 5350
Wire Wire Line
	6900 5250 6350 5250
Wire Wire Line
	6900 5150 6350 5150
Wire Wire Line
	6900 5050 6350 5050
Wire Wire Line
	5850 5350 5300 5350
Wire Wire Line
	5850 5250 5300 5250
Wire Wire Line
	5850 5150 5300 5150
Wire Wire Line
	5850 5050 5300 5050
$Comp
L Conn_02x04_Odd_Even J7
U 1 1 5ADB2304
P 6050 5150
AR Path="/5AE0048B/5ADB2304" Ref="J7"  Part="1" 
AR Path="/5AE01390/5ADB2304" Ref="J14"  Part="1" 
AR Path="/5AE0244B/5ADB2304" Ref="J21"  Part="1" 
AR Path="/5AE03F44/5ADB2304" Ref="J28"  Part="1" 
AR Path="/5AE04D35/5ADB2304" Ref="J35"  Part="1" 
AR Path="/5AE0593D/5ADB2304" Ref="J42"  Part="1" 
AR Path="/5AE05E7C/5ADB2304" Ref="J49"  Part="1" 
AR Path="/5AE07945/5ADB2304" Ref="J56"  Part="1" 
AR Path="/5AE094A1/5ADB2304" Ref="J63"  Part="1" 
AR Path="/5AE0AD1B/5ADB2304" Ref="J70"  Part="1" 
AR Path="/5AE0C60A/5ADB2304" Ref="J77"  Part="1" 
AR Path="/5AE0F6A9/5ADB2304" Ref="J84"  Part="1" 
F 0 "J84" H 6100 5350 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 6100 4850 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecSMM" H 6050 5150 50  0001 C CNN
F 3 "" H 6050 5150 50  0001 C CNN
	1    6050 5150
	1    0    0    -1  
$EndComp
Text HLabel 6900 5700 2    60   Input ~ 0
V7
Text HLabel 6900 5800 2    60   Input ~ 0
V6
Text HLabel 6900 5900 2    60   Input ~ 0
V5
Text HLabel 6900 6000 2    60   Input ~ 0
V4
Text HLabel 5300 6000 0    60   Input ~ 0
V3
Text HLabel 5300 5900 0    60   Input ~ 0
V2
Text HLabel 5300 5800 0    60   Input ~ 0
V1
Text HLabel 5300 5700 0    60   Input ~ 0
V0
Wire Wire Line
	6900 6000 6350 6000
Wire Wire Line
	6900 5900 6350 5900
Wire Wire Line
	6900 5800 6350 5800
Wire Wire Line
	6900 5700 6350 5700
Wire Wire Line
	5850 6000 5300 6000
Wire Wire Line
	5850 5900 5300 5900
Wire Wire Line
	5850 5800 5300 5800
Wire Wire Line
	5850 5700 5300 5700
$Comp
L Conn_02x04_Odd_Even J9
U 1 1 5ADB231A
P 6050 5800
AR Path="/5AE0048B/5ADB231A" Ref="J9"  Part="1" 
AR Path="/5AE01390/5ADB231A" Ref="J16"  Part="1" 
AR Path="/5AE0244B/5ADB231A" Ref="J23"  Part="1" 
AR Path="/5AE03F44/5ADB231A" Ref="J30"  Part="1" 
AR Path="/5AE04D35/5ADB231A" Ref="J37"  Part="1" 
AR Path="/5AE0593D/5ADB231A" Ref="J44"  Part="1" 
AR Path="/5AE05E7C/5ADB231A" Ref="J51"  Part="1" 
AR Path="/5AE07945/5ADB231A" Ref="J58"  Part="1" 
AR Path="/5AE094A1/5ADB231A" Ref="J65"  Part="1" 
AR Path="/5AE0AD1B/5ADB231A" Ref="J72"  Part="1" 
AR Path="/5AE0C60A/5ADB231A" Ref="J79"  Part="1" 
AR Path="/5AE0F6A9/5ADB231A" Ref="J86"  Part="1" 
F 0 "J86" H 6100 6000 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 6100 5500 50  0000 C CNN
F 2 "samtec_tmm:Pin_Header_Straight_2x04_Pitch2.00mm_samtecSMM" H 6050 5800 50  0001 C CNN
F 3 "" H 6050 5800 50  0001 C CNN
	1    6050 5800
	1    0    0    -1  
$EndComp
NoConn ~ 6450 4200
NoConn ~ 6450 4300
Wire Notes Line
	2900 4850 2900 6250
Wire Notes Line
	2900 6250 7200 6250
Wire Notes Line
	7200 6250 7200 4850
Wire Notes Line
	7200 4850 2900 4850
Text Notes 2900 4800 0    60   ~ 12
Configuration card pins
Wire Notes Line
	2900 3400 2900 4600
Wire Notes Line
	2900 4600 7100 4600
Wire Notes Line
	7100 4600 7100 3400
Wire Notes Line
	7100 3400 2900 3400
Text Notes 2900 3350 0    60   ~ 12
Daughter Card Interface
Wire Notes Line
	4200 2200 4200 3200
Wire Notes Line
	4200 3200 5700 3200
Wire Notes Line
	5700 3200 5700 2200
Wire Notes Line
	5700 2200 4200 2200
Text Notes 4200 2150 0    60   ~ 12
Output Connector
$EndSCHEMATC
